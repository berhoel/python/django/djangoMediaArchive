from _typeshed import Incomplete
from django.db.models.base import ModelBase

from .query import PolymorphicQuerySet as PolymorphicQuerySet
from .managers import PolymorphicManager as PolymorphicManager

POLYMORPHIC_SPECIAL_Q_KWORDS: Incomplete
DUMPDATA_COMMAND: Incomplete

class ManagerInheritanceWarning(RuntimeWarning): ...

class PolymorphicModelBase(ModelBase):
    def __new__(self, model_name, bases, attrs, **kwargs): ...
    @classmethod
    def call_superclass_new_method(self, model_name, bases, attrs, **kwargs): ...
    @classmethod
    def validate_model_fields(self, new_class) -> None: ...
    @classmethod
    def validate_model_manager(self, manager, model_name, manager_name): ...
    @property
    def base_objects(self): ...
