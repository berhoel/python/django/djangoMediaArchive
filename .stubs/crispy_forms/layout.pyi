from dataclasses import dataclass

from _typeshed import Incomplete
from crispy_forms.utils import (
    TEMPLATE_PACK as TEMPLATE_PACK,
    flatatt as flatatt,
    render_field as render_field,
)

@dataclass
class Pointer:
    positions: list[int]
    name: str
    def __init__(self, positions, name) -> None: ...

class TemplateNameMixin:
    def get_template_name(self, template_pack): ...

class LayoutObject(TemplateNameMixin):
    def __getitem__(self, slice): ...
    def __setitem__(self, slice, value) -> None: ...
    def __delitem__(self, slice) -> None: ...
    def __len__(self) -> int: ...
    def __getattr__(self, name): ...
    def get_field_names(self, index: Incomplete | None = None): ...
    def get_layout_objects(
        self,
        *LayoutClasses,
        index: Incomplete | None = None,
        max_level: int = 0,
        greedy: bool = False,
    ): ...
    def get_rendered_fields(self, form, context, template_pack=..., **kwargs): ...

class Layout(LayoutObject):
    fields: Incomplete
    def __init__(self, *fields) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class ButtonHolder(LayoutObject):
    template: str
    fields: Incomplete
    css_id: Incomplete
    css_class: Incomplete
    def __init__(
        self,
        *fields,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class BaseInput(TemplateNameMixin):
    template: str
    field_classes: str
    name: Incomplete
    value: Incomplete
    id: Incomplete
    attrs: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        name,
        value,
        *,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class Submit(BaseInput):
    input_type: str
    field_classes: str

class Button(BaseInput):
    input_type: str
    field_classes: str

class Hidden(BaseInput):
    input_type: str
    field_classes: str

class Reset(BaseInput):
    input_type: str
    field_classes: str

class Fieldset(LayoutObject):
    template: str
    fields: Incomplete
    legend: Incomplete
    css_class: Incomplete
    css_id: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        legend,
        *fields,
        css_class: Incomplete | None = None,
        css_id: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class MultiField(LayoutObject):
    template: str
    field_template: str
    fields: Incomplete
    label_html: Incomplete
    label_class: Incomplete
    css_class: Incomplete
    css_id: Incomplete
    help_text: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        label,
        *fields,
        label_class: Incomplete | None = None,
        help_text: Incomplete | None = None,
        css_class: Incomplete | None = None,
        css_id: Incomplete | None = None,
        template: Incomplete | None = None,
        field_template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class Div(LayoutObject):
    template: str
    css_class: Incomplete
    fields: Incomplete
    css_id: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        *fields,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class Row(Div):
    template: str

class Column(Div):
    template: str

class HTML:
    html: Incomplete
    def __init__(self, html) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class Field(LayoutObject):
    template: str
    attrs: Incomplete
    fields: Incomplete
    wrapper_class: Incomplete
    def __init__(
        self,
        *fields,
        css_class: Incomplete | None = None,
        wrapper_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(
        self,
        form,
        context,
        template_pack=...,
        extra_context: Incomplete | None = None,
        **kwargs,
    ): ...

class MultiWidgetField(Field):
    fields: Incomplete
    attrs: Incomplete
    template: Incomplete
    wrapper_class: Incomplete
    def __init__(
        self,
        *fields,
        attrs: Incomplete | None = None,
        template: Incomplete | None = None,
        wrapper_class: Incomplete | None = None,
    ) -> None: ...
