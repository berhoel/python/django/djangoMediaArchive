from _typeshed import Incomplete

from .utils import (
    TEMPLATE_PACK as TEMPLATE_PACK,
    flatatt as flatatt,
    render_field as render_field,
)
from .layout import (
    Div as Div,
    Field as Field,
    LayoutObject as LayoutObject,
    TemplateNameMixin as TemplateNameMixin,
)

class PrependedAppendedText(Field):
    template: str
    field: Incomplete
    appended_text: Incomplete
    prepended_text: Incomplete
    active: Incomplete
    input_size: Incomplete
    def __init__(
        self,
        field,
        prepended_text: Incomplete | None = None,
        appended_text: Incomplete | None = None,
        input_size: Incomplete | None = None,
        *,
        active: bool = False,
        css_class: Incomplete | None = None,
        wrapper_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(
        self,
        form,
        context,
        template_pack=...,
        extra_context: Incomplete | None = None,
        **kwargs,
    ): ...

class AppendedText(PrependedAppendedText):
    text: Incomplete
    def __init__(
        self,
        field,
        text,
        *,
        input_size: Incomplete | None = None,
        active: bool = False,
        css_class: Incomplete | None = None,
        wrapper_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...

class PrependedText(PrependedAppendedText):
    text: Incomplete
    def __init__(
        self,
        field,
        text,
        *,
        input_size: Incomplete | None = None,
        active: bool = False,
        css_class: Incomplete | None = None,
        wrapper_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...

class FormActions(LayoutObject):
    template: str
    fields: Incomplete
    id: Incomplete
    css_class: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        *fields,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class InlineCheckboxes(Field):
    template: str
    def render(self, form, context, template_pack=..., **kwargs): ...

class InlineRadios(Field):
    template: str
    def render(self, form, context, template_pack=..., **kwargs): ...

class FieldWithButtons(Div):
    template: str
    field_template: str
    input_size: Incomplete
    def __init__(
        self,
        *fields,
        input_size: Incomplete | None = None,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(
        self,
        form,
        context,
        template_pack=...,
        extra_context: Incomplete | None = None,
        **kwargs,
    ): ...

class StrictButton(TemplateNameMixin):
    template: str
    field_classes: str
    content: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        content,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class Container(Div):
    css_class: str
    name: Incomplete
    active: Incomplete
    css_id: Incomplete
    def __init__(
        self,
        name,
        *fields,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        active: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def __contains__(self, field_name) -> bool: ...

class ContainerHolder(Div):
    def first_container_with_errors(self, errors): ...
    def open_target_group_for_form(self, form): ...

class Tab(Container):
    css_class: str
    link_template: str
    def render_link(self, template_pack=..., **kwargs): ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class TabHolder(ContainerHolder):
    template: str
    def render(self, form, context, template_pack=..., **kwargs): ...

class AccordionGroup(Container):
    template: str
    data_parent: str

class Accordion(ContainerHolder):
    template: str
    css_id: Incomplete
    def __init__(
        self,
        *accordion_groups,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class Alert(Div):
    template: str
    css_class: str
    content: Incomplete
    dismiss: Incomplete
    def __init__(
        self,
        content,
        dismiss: bool = True,
        block: bool = False,
        css_id: Incomplete | None = None,
        css_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...

class UneditableField(Field):
    template: str
    attrs: Incomplete
    def __init__(
        self,
        field,
        css_class: Incomplete | None = None,
        wrapper_class: Incomplete | None = None,
        template: Incomplete | None = None,
        **kwargs,
    ) -> None: ...

class InlineField(Field):
    template: str

class Modal(LayoutObject):
    template: str
    fields: Incomplete
    css_id: Incomplete
    css_class: Incomplete
    title: Incomplete
    title_id: Incomplete
    title_class: Incomplete
    flat_attrs: Incomplete
    def __init__(
        self,
        *fields,
        template: Incomplete | None = None,
        css_id: str = "modal_id",
        title: str = "Modal Title",
        title_id: str = "modal_title_id",
        css_class: Incomplete | None = None,
        title_class: Incomplete | None = None,
        **kwargs,
    ) -> None: ...
    def render(self, form, context, template_pack=..., **kwargs): ...
