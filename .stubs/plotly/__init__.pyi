from plotly import (
    io as io,
    data as data,
    tools as tools,
    utils as utils,
    colors as colors,
    offline as offline,
    graph_objs as graph_objs,
)
from plotly.version import __version__ as __version__

__all__ = [
    "graph_objs",
    "graph_objects",
    "tools",
    "utils",
    "offline",
    "colors",
    "io",
    "data",
    "__version__",
]

# Names in __all__ with no definition:
#   graph_objects
