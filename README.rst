djangoMediaArchive
==================

Django app for recording media (DVD, CD, record) collections and their
use.

Detailed documentation is in the “docs” directory.

Quick start
-----------

1. Add “djangoMediaArchive” to your INSTALLED_APPS setting like this::

     INSTALLED_APPS = [
         ...
         ‘berhoel.django.media’,
     ]

2. Include the djangoMediaArchive URLconf in your project urls.py like
   this::

     path(‘media/’, include(‘berhoel.django.media.urls’)),

3. Run ``python manage.py migrate`` to create the djangoMediaArchive
   models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a inspect media data (you’ll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/media/ work with the media information.
