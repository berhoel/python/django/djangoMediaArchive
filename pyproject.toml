[tool.poetry]

name = "djangoMediaArchive"
version = "0.1.4"

description = "A Django app to manage media usage."

packages = [{ include = "berhoel" }]
exclude = ["**/.#*.py", "**/#*.py#"]

authors = ["Berthold Höllmann <berthold@xn--hllmanns-n4a.de>"]
license = "MIT"

repository = "https://gitlab.com/berhoel/python/django/djangoMediaArchive.git"
homepage = "https://python.xn--hllmanns-n4a.de/djangoMediaArchive/"
documentation = "https://python.xn--hllmanns-n4a.de/djangoMediaArchive/"

readme = "README.rst"

classifiers = [
  "Environment :: Web Environment",
  "Framework :: Django",
  "Intended Audience :: End users",
  "License :: OSI Approved :: MIT License",
  "Operating System :: OS Independent",
  "Programming Language :: Python",
  "Topic :: Internet :: WWW/HTTP",
  "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
]

[tool.poetry.dependencies]
python = "^3.10"
cddb = { path = "/home/hoel/work/cddb-py/" }
django = "*"
django-crispy-forms = "*"
#django-polymorphic = "*"
django-polymorphic = {git="https://github.com/jazzband/django-polymorphic.git",rev="v4.0.0a"}
django-extensions = "*"
django-fontawesome-5 = "*"
psycopg2 = "*"
plotly = "*"
numpy = "*"
pandas = "*"
django-bootstrap-v5 = "*"
crispy-bootstrap5 = "*"

[tool.poetry.group.dev.dependencies]
berhoel-sphinx-settings = { git = "https://gitlab.com/berhoel/python/berhoel-sphinx-settings.git", rev = "main" }
black = "*"
ipython="*"
isort = "*"
lxml-stubs = "*"
mypy = "*"
pdbp = "*"
pytest = "*"
pytest-benchmark = "*"
pytest-cache = "*"
pytest-cov = "*"
pytest-isort = "*"
pytest-env = "*"
pytest-mock = "*"
pytest-ruff = "*"
pytest-runner = "*"
pytest-sugar = "*"
rope = "*"
ruff = "*"
sphinx-argparse-cli = "*"
tox = "*"
django-stubs = {extras = ["compatible-mypy"], version = "*"}
pandas-stubs = "^2.2.2.240807"
typing-extensions = "^4.12.2"

[tool.isort]
profile = "django"
dedup_headings = true
include_trailing_comma = true
float_to_top = true
namespace_packages = ['berhoel']
force_grid_wrap = 0
balanced_wrapping = true
use_parentheses = true
length_sort = true
line_length = 88
combine_as_imports = true
known_first_party = ["berhoel"]
known_third_party = ["pytest"]

[tool.ruff]
namespace-packages = ["docs", 'berhoel']
line-length = 88

[tool.ruff.lint]
select = ["ALL"]
ignore = ["ANN101", "D211", "D213", "RUF200", "ISC001", "COM812"]

[tool.ruff.lint.flake8-annotations]
allow-star-arg-any = true
mypy-init-return = true

[tool.ruff.lint.pydocstyle]
convention = "numpy"

[tool.ruff.format]
# Enable auto-formatting of code examples in docstrings. Markdown,
# reStructuredText code/literal blocks and doctests are all supported.
#
# This is currently disabled by default, but it is planned for this
# to be opt-out in the future.
docstring-code-format = true

# Set the line length limit used when formatting code snippets in
# docstrings.
#
# This only has an effect when the `docstring-code-format` setting is
# enabled.
docstring-code-line-length = "dynamic"

[tool.ruff.lint.isort]
force-sort-within-sections = true
force-wrap-aliases = true
combine-as-imports = true
section-order = [
  "future",
  "standard-library",
  "third-party",
  "first-party",
  "local-folder",
]
default-section = "third-party"
known-first-party = ["berhoel"]
known-third-party = ["pytest"]

[tool.ruff.lint.flake8-quotes]
docstring-quotes = "double"

[tool.mypy]
mypy_path = "$MYPY_CONFIG_FILE_DIR/.stubs/"
plugins = ["mypy_django_plugin.main"]

[tool.django-stubs]
django_settings_module = "berhoel.django.media.settings"

[tool.tox]
legacy_tox_ini = """

[tox]
isolated_build = true
skip_missing_interpreters = true
envlist = py3{10, 11, 12, 13, 14}

[testenv]
deps =
   pdbp
   pytest
   pytest-benchmark
   pytest-cov
   pytest-mock
   pytest-ruff
   pytest-runner
   pytest-sugar
   rope
   xdoctest

whitelist_externals = poetry

commands =
   poetry install
   poetry run pytest
"""

[tool.pytest.ini_options]
junit_family = "xunit2"
doctest_encoding = "UTF-8"
minversion = "7.0"
addopts = "--ff --pdb -s --cov=berhoel --cov-report=term --cov-report=html --cov-branch --ruff"
testpaths = ["berhoel"]
env = [
  "D:EXTERNAL_RUN=1",
  "D:DJANGO_SETTINGS_MODULE=berhoel.django.media.settings",
]

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
