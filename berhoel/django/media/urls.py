"""Media URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please
`see <https://docs.djangoproject.com/en/2.1/topics/http/urls/>`_

Examples
--------
Function views

1. Add an import::

     from my_app import views

2. Add a URL to urlpatterns::

      path("", views.home, name="home")

Class-based views

1. Add an import::

      from other_app.views import Home

2. Add a URL to urlpatterns::

      path("", Home.as_view(), name="home")

Including another URLconf

1. Import the include() function::

      from django.urls import include, path

2. Add a URL to urlpatterns::

      path("blog/", include("blog.urls"))
"""

# Django library imports.
from django.urls import path

# Local library imports.
from . import views

__date__ = "2022/07/31 16:47:43 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

urlpatterns = [
    path(r"", views.IndexView.as_view(), name="index"),
    path(r"graph/", views.Graph.as_view(), name="graph"),
    path(r"cal/", views.calendar_view, name="calendar"),
    path(r"cal/<int:year>/<int:month>/", views.calendar_view, name="calendar"),
    path(
        "<uuid:pk>/",
        views.SeenUpdateView.as_view(),
        name="view_seen",
    ),
    path(r"add_person/", views.PersonCreateView.as_view()),
    path(
        "<uuid:pk>/edit_person/",
        views.PersonUpdateView.as_view(),
        name="edit_person",
    ),
    path(
        "<uuid:pk>/edit_seen/",
        views.SeenUpdateView.as_view(),
        name="edit_seen",
    ),
    path(
        "delete_seen/<uuid:pk>",
        views.SeenDeleteView.as_view(),
        name="delete_seen",
    ),
]
