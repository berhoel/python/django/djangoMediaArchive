"""Forms for media django project."""

from __future__ import annotations

import logging
from typing import TYPE_CHECKING, Any

from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Fieldset, Layout, Submit
from django import forms
from django.forms.utils import ErrorList

from .models import Person, Seen
from .widgets import BootstrapDatePickerInput

if TYPE_CHECKING:
    from django.core.files.uploadedfile import UploadedFile
    from django.utils.datastructures import MultiValueDict

LOGGER = logging.getLogger(__name__)

__date__ = "2024/08/08 22:55:08 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


class PersonForm(forms.ModelForm):
    """Form for person entries."""

    class Meta:
        """Class meta data."""

        model = Person
        fields = ("name",)

    def __init__(  # noqa:PLR0913
        self,
        data: dict[str, Any] | None = None,
        files: MultiValueDict[str, UploadedFile] | None = None,
        auto_id: bool | str = "id_%s",
        prefix: str | None = None,
        initial: dict[str, Any] | None = None,
        error_class: type[ErrorList] = ErrorList,
        label_suffix: str | None = None,
        empty_permitted: bool = False,  # noqa:FBT001,FBT002
        field_order: Any | None = None,  # noqa:ANN401
        use_required_attribute: bool | None = None,
        renderer: Any | None = None,  # noqa:ANN401
    ):
        super().__init__(
            data,
            files,
            auto_id,
            prefix,
            initial,
            error_class,
            label_suffix,
            empty_permitted,
            field_order,
            use_required_attribute,
            renderer,
        )
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.add_input(Submit("submit", "Save person"))


class SeenForm(forms.ModelForm):
    """Form for seen entries."""

    date = forms.DateField(
        input_formats=["%d/%m/%Y"], widget=BootstrapDatePickerInput()
    )

    class Meta:
        """Class meta data."""

        model = Seen
        fields = ("date", "watch_item")

    def __init__(  # noqa:PLR0913
        self,
        data: dict[str, Any] | None = None,
        files: MultiValueDict[str, UploadedFile] | None = None,
        auto_id: bool | str = "id_%s",
        prefix: str | None = None,
        initial: dict[str, Any] | None = None,
        error_class: type[ErrorList] = ErrorList,
        label_suffix: str | None = None,
        empty_permitted: bool = False,  # noqa:FBT001,FBT002
        field_order: Any | None = None,  # noqa:ANN401
        use_required_attribute: bool | None = None,
        renderer: Any | None = None,  # noqa:ANN401
    ):
        super().__init__(
            data,
            files,
            auto_id,
            prefix,
            initial,
            error_class,
            label_suffix,
            empty_permitted,
            field_order,
            use_required_attribute,
            renderer,
        )
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                "",
                "date",
                "watch_item",
            ),
            FormActions(
                Submit("update", "Submit Seen", css_class="btn-success"),
                Submit("cancel", "Cancel", css_class="btn-secondary"),
            ),
        )


class SeenDeleteForm(forms.ModelForm):
    """Form for seen delete."""

    date = forms.DateField(
        input_formats=["%d/%m/%Y"], widget=BootstrapDatePickerInput()
    )

    class Meta:
        """Class meta data."""

        model = Seen
        fields = ("date", "watch_item")

    def __init__(  # noqa:PLR0913
        self,
        data: dict[str, Any] | None = None,
        files: MultiValueDict[str, UploadedFile] | None = None,
        auto_id: bool | str = "id_%s",
        prefix: str | None = None,
        initial: dict[str, Any] | None = None,
        error_class: type[ErrorList] = ErrorList,
        label_suffix: str | None = None,
        empty_permitted: bool = False,  # noqa:FBT001,FBT002
        field_order: Any | None = None,  # noqa:ANN401
        use_required_attribute: bool | None = None,
        renderer: Any | None = None,  # noqa:ANN401
    ):
        super().__init__(
            data,
            files,
            auto_id,
            prefix,
            initial,
            error_class,
            label_suffix,
            empty_permitted,
            field_order,
            use_required_attribute,
            renderer,
        )
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                "",
                "date",
                "watch_item",
            ),
            FormActions(
                Submit("delete", "Delete Seen", css_class="btn-danger"),
                Submit("cancel", "Cancel", css_class="btn-secondary"),
            ),
        )
