"""Django apps."""

from django.apps import AppConfig


class Djangomediaarchive(AppConfig):
    """Main application for this project."""

    default_auto_field = "django.db.models.AutoField"
    name = "berhoel.django.media"
