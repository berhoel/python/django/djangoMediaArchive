"""Django admin for series."""

from django.contrib import admin

from .models import (
    DVD,
    Broadcast,
    Film,
    Media,
    Person,
    Purchase,
    Season,
    Seen,
    Sell,
    Series,
    SeriesEpisode,
    Streaming,
    Subscription,
    SubscriptionPayment,
    TheatreVisit,
    WatchItem,
)

__date__ = "2024/08/08 14:27:55 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2015 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


# Register your models here.
admin.site.register(Media)
admin.site.register(DVD)
admin.site.register(Person)
admin.site.register(Sell)
admin.site.register(Purchase)
admin.site.register(WatchItem)
admin.site.register(Film)
admin.site.register(Series)
admin.site.register(Season)
admin.site.register(SeriesEpisode)
admin.site.register(Broadcast)
admin.site.register(TheatreVisit)
admin.site.register(SubscriptionPayment)
admin.site.register(Subscription)
admin.site.register(Streaming)
admin.site.register(Seen)
