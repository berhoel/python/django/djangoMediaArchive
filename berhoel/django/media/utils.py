"""Utilities for media."""

from __future__ import annotations

import calendar
from typing import ClassVar

__date__ = "2024/08/09 18:18:51 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


class Calendar(calendar.HTMLCalendar):
    """Calendar for django media archive."""

    cssclasses_weekday_head: ClassVar = [
        "mon text-bold",
        "tue text-bold",
        "wed text-bold",
        "thu text-bold",
        "fri text-bold",
        "sat text-bold",
        "sun text-bold",
    ]
    cssclasses: ClassVar = [
        "mon",
        "tue",
        "wed",
        "thu",
        "fri",
        "sat table-info",
        "sun table-info",
    ]
    cssclass_month = "table table-sm table-striped table-bordered"
    cssclass_thead = "table-dark"

    def __init__(self, firstweekday: int = 0):
        super().__init__(firstweekday=firstweekday)
        self.seens = None

    def formatday(self, day: int, weekday: int) -> str:
        """Format calendar day entry."""
        if day != 0:
            return f"""\
{{% spaceless %}}
<td class="{self.cssclasses[weekday]} col-1">
  <span class="date"><b>{day}</b>
    <div class="small">
     {{% for item in seens_per_day_d{day} %}}
       <button class="btn btn-outline-secondary btn-sm"
                role="button"
                data-bs-toggle="collapse"
               data-bs-target="#collapse_d{day}_{{{{ forloop.counter0 }}}}"
                aria-expanded="false"
               aria-controls="collapse_d{day}_{{{{ forloop.counter0 }}}}">
          {{% if item.watch_item.item_type is watch_item_types.SeriesEpisode %}}
            {{{{ item.watch_item.season.series.title }}}},
          {{% endif %}}
          <a href="{{{{ url|safe }}}}">{{{{ item.watch_item.title }}}}</a>
        </button>
       <div class="collapse" id="collapse_d{day}_{{{{ forloop.counter0 }}}}">
          <div class="card card-body">
          {{% if item.watch_item.item_type is watch_item_types.SeriesEpisode %}}
            {{% if item.watch_item.sub_episode %}}
              s{{{{ item.watch_item.season.season }}}}e{{{{ item.watch_item.episode }}}}.{{{{ item.watch_item.sub_episode }}}}
            {{% else %}}
              s{{{{ item.watch_item.season.season }}}}e{{{{ item.watch_item.episode }}}}
            {{% endif %}}
          {{% endif %}}
          <button role="button"
                  class="bs-modal delete-seen btn btn-sm btn-danger"
                  data-bs-toggle="collapse"
                  href="#collapse_delete_{{{{ item.id }}}}"
                  aria-expanded="false"
                  aria-controls="collapse_delete_{{{{ item.id }}}}">
            {{% fa5_icon 'trash' %}}
          </button>
          <a href="{{{{ item.watch_item.imdb_url|safe }}}}"
             target="_blank"
             role="button"
             class="bs-modal btn btn-sm btn-info">
            <img title="Infos on IMDB" src="/static/media/IMDB.png"
                 alt="IMDB"
                 width="15"
                 height="15"/></a>
          <a href="medium/{{{{ item.watch_item.medium.id }}}}/">({{{{ item.media_type }}}})</a>
          <div class="collapse" id="collapse_delete_{{{{ item.id }}}}">
            <div class="card card-body">
              <form method="post" action="{{% url \'delete_seen\' item.id %}}">{{% csrf_token %}}
                <h3>Delete Seen</h3>
                <p class="delete-text">Are you sure you want to delete seen entry with title
                <strong>
                  {{% if item.watch_item.item_type is watch_item_types.SeriesEpisode %}}
                    {{{{ item.watch_item.season.series.title }}}}, {{{{ item.watch_item.title }}}}
                    {{% if item.watch_item.sub_episode %}}
                      s{{{{ item.watch_item.season.season }}}}e{{{{ item.watch_item.episode }}}}.{{{{ item.watch_item.sub_episode }}}}
                    {{% else %}}
                      s{{{{ item.watch_item.season.season }}}}e{{{{ item.watch_item.episode }}}}
                    {{% endif %}}
                  {{% else %}}
                    {{{{ item.watch_item.title }}}}
                  {{% endif %}}
                </strong>?</p>
                <button class="submit btn btn-danger">Delete</button>
              </form>
            </div>
          </div>
        </div>
      </div><br/>{{% endfor %}}
    </div>
  </span>
</td>{{% endspaceless %}}"""  # noqa: E501

        return f"""\
<td class="{self.cssclasses[weekday]} col-1">
  <span class="date">&nbsp;</span>
</td>"""

    def formatweekday(self, day: int) -> str:
        """Return a weekday name as a table header."""
        cssclass = self.cssclasses_weekday_head[day]
        day_abbr = calendar.day_abbr[day]
        return f'<th class="{cssclass} col-1" scope="col">{day_abbr}</th>'

    def formatmonth(
        self,
        theyear: int,
        themonth: int,
        withyear: bool = True,  # noqa:FBT001,FBT002
    ) -> str:
        """Return formatted month entry."""
        res = [
            f"""\
<table table-layout="fixed" class="{ self.cssclass_month }">
  <thead class="{ self.cssclass_thead }">
    {self.formatmonthname(
            theyear,
            themonth,
            withyear=withyear)}
    {self.formatweekheader()}
  </thead>"""
        ]
        res.extend(
            self.formatweek(w)  # type: ignore[arg-type]
            for w in self.monthdays2calendar(theyear, themonth)
        )
        res.append("</table>")
        return "\n".join(res)
