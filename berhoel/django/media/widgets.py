"""Widgets for media."""

from __future__ import annotations

from django.forms import DateInput, DateTimeInput

__date__ = "2024/08/09 18:20:44 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


class BootstrapDatePickerInput(DateInput):
    """Date picker with bootstrap."""

    template_name = "widgets/bootstrap_datepicker.html"

    def get_context(
        self, name: str, value: str, attrs: dict[str, str] | None
    ) -> dict[str, str]:
        """Get current context information."""
        datepicker_id = f"datepicker_{name}"
        if attrs is None:
            attrs = {}
        attrs.update(
            {
                "data-target": f"#{datepicker_id}",
                "class": "form-control datepicker-input",
            }
        )
        context = super().get_context(name, value, attrs)
        context["widget"]["datepicker_id"] = datepicker_id
        return context


class BootstrapDateTimePickerInput(DateTimeInput):
    """Time picker with bootstrap."""

    template_name = "widgets/bootstrap_datetimepicker.html"

    def get_context(
        self, name: str, value: str, attrs: dict[str, str] | None
    ) -> dict[str, str]:
        """Get current context information."""
        datetimepicker_id = f"datetimepicker_{name}"
        if attrs is None:
            attrs = {}
        attrs.update(
            {
                "data-target": f"#{datetimepicker_id}",
                "class": "form-control datetimepicker-input",
            }
        )
        context = super().get_context(name, value, attrs)
        context["widget"]["datetimepicker_id"] = datetimepicker_id
        return context
