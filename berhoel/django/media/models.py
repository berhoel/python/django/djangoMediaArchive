"""Django models for series application."""

import enum
from typing import ClassVar
import uuid

from django.db import models
from django.db.models.fields import Field

from polymorphic.models import PolymorphicModel  # isort: skip

__date__ = "2024/08/09 18:28:21 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2015 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


# Create your models here.
class MyDjangoEnum(enum.Enum):
    """Prepare Enums for Django."""

    @property
    def do_not_call_in_templates(self) -> bool:
        """Prevent instances from being called in templates."""
        return True


class MediaTypes(MyDjangoEnum):
    """Media types."""

    MEDIA = enum.auto()
    DVD = enum.auto()
    BROADCAST = enum.auto()
    RENTAL_VIDEO = enum.auto()
    THEATRE_VISIT = enum.auto()
    STREAMING = enum.auto()

    def __str__(self) -> str:
        """Return string representation."""
        return {
            "MEDIA": "Media",
            "DVD": "DVD",
            "BROADCAST": "Broadcast",
            "RENTAL_VIDEO": "RentalVideo",
            "THEATRE_VISIT": "TheatreVisit",
            "STREAMING": "Streaming",
        }[self.name]


class WatchItemTypes(MyDjangoEnum):
    """Types for watch items."""

    WATCH_ITEM = enum.auto()
    FILM = enum.auto()
    SERIES_EPISODE = enum.auto()

    def __str__(self) -> str:
        """Return string representation."""
        return {
            "WATCH_ITEM": "WatchItem",
            "FILM": "Film",
            "SERIES_EPISODE": "SeriesEpisode",
        }[self.name]


class Media(PolymorphicModel):
    """Base class for mapping diffenrent media."""

    _type = MediaTypes.MEDIA

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField("name", max_length=255)
    family = models.BooleanField("family?", default=False, blank=True)
    season = models.IntegerField(blank=True, null=True)
    sub_season = models.IntegerField(blank=True, null=True)

    class Meta:
        """Configure Django model class."""

        verbose_name = "media"
        verbose_name_plural = "media"
        ordering: ClassVar = ["name", "season", "sub_season"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name}"

    @property
    def item_type(self) -> enum.Enum:
        """Return media type."""
        return self._type


class DVD(Media):
    """DVD or DVD set."""

    _type = MediaTypes.DVD

    disks = models.IntegerField(null=True)
    adver_name = models.CharField("adver_name", max_length=255)

    def __str__(self) -> str:
        """Return string representation."""
        res = [
            f"{self.name}",
            f"{self.adver_name}",
            f"{Purchase.objects.filter(media=self)[0].date}",
        ]
        if self.season is not None:
            if self.sub_season is not None:
                res.append(f"season {self.season}.{self.sub_season}")
            else:
                res.append(f"season {self.season}")
        return " / ".join(res)


class Person(models.Model):
    """General person information."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["name"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name}"


class Purchase(models.Model):
    """Record purchase of media item."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    vendor = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="purchases",
        related_query_name="purchase",
    )
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    date = models.DateField(blank=True)
    media = models.ManyToManyField(
        Media,
        related_name="purchases",
        related_query_name="purchase",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["-date"]

    def __str__(self) -> str:
        """Return string representation."""
        res = f"{self.date} "
        if self.vendor is not None:
            res += f"from {self.vendor.name} for {self.price}€ "
        else:
            res += f"for {self.price}€ "
        return res + f"({' / '.join(i.name for i in self.media.all())})"


class Sell(models.Model):
    """Record selling of media item."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    buyer = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        related_name="sells",
        related_query_name="sell",
    )
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    date = models.DateField(blank=True)
    dvds = models.ManyToManyField(
        DVD,
        related_name="sells",
        related_query_name="sell",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["-date"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.buyer} | {self.date} | {self.price}"


class WatchItem(PolymorphicModel):
    """Representing a viewable item."""

    _type = WatchItemTypes.WATCH_ITEM

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    medium = models.ForeignKey(
        Media,
        on_delete=models.PROTECT,
        related_name="watch_items",
        related_query_name="watch_item",
    )
    dvd_index = models.IntegerField(blank=True, null=True)
    imdb_url = models.URLField(blank=True, default="")
    title = models.CharField(max_length=255)
    desc = models.CharField(max_length=2048, blank=True, default="")

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.title}"

    @property
    def item_type(self) -> WatchItemTypes:
        """Return watch item type."""
        return self._type


class Film(WatchItem):
    """Watching a film."""

    _type = WatchItemTypes.FILM

    options = models.CharField(max_length=255, blank=True, default="")

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.title}"


class Series(models.Model):
    """Information of series."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255)
    imdb_url = models.URLField(blank=True, default="")
    order_no = models.IntegerField()
    is_active = models.BooleanField(default=False)

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["order_no"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.title=} {self.imdb_url=} {self.order_no=} {self.is_active=}"


class Season(models.Model):
    """Information on series season as in DVD, broadcast, or streaming."""

    series = models.ForeignKey(
        Series,
        on_delete=models.PROTECT,
        related_name="seasons",
        related_query_name="season",
    )
    season = models.IntegerField()
    subseason = models.IntegerField(blank=True, null=True)

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.series=} {self.season=} {self.subseason=}"


class SeriesEpisode(WatchItem):
    """Watching an episode of an series."""

    _type = WatchItemTypes.SERIES_EPISODE

    season: Field = models.ForeignKey(
        Season,
        on_delete=models.PROTECT,
        related_name="series_episodes",
        related_query_name="series_episode",
    )
    episode: Field = models.IntegerField()
    sub_episode: Field = models.IntegerField(blank=True, null=True)

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.title}"


class Broadcast(Media):
    """Broadcast event of media."""

    _type = MediaTypes.BROADCAST

    broadcast_service: Field = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        related_name="broadcasts",
        related_query_name="broadcast",
    )

    def __str__(self) -> str:
        """Return string representation."""
        res = [f"{self.name}", f"{self.broadcast_service.name}"]
        if self.season is not None:
            if self.sub_season is not None:
                res.append(f"season {self.season}.{self.sub_season}")
            else:
                res.append(f"season {self.season}")
        return " / ".join(res)


class RentalVideo(Media):
    """Seen media in cinema."""

    _type = MediaTypes.RENTAL_VIDEO

    company: Field = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        related_name="rental_videos",
        related_query_name="rental_video",
    )
    price: Field = models.DecimalField(
        max_digits=8, decimal_places=2, blank=True, null=True
    )

    def __str__(self) -> str:
        """Return string representation."""
        res = [f"{self.name}", f"{self.company.name}"]
        if self.season is not None:
            if self.sub_season is not None:
                res.append(f"season {self.season}.{self.sub_season}")
            else:
                res.append(f"season {self.season}")
        return " / ".join(res)


class TheatreVisit(Media):
    """Seen media in cinema."""

    _type = MediaTypes.THEATRE_VISIT

    theatre: Field = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        related_name="theatre_visits",
        related_query_name="theatre_visit",
    )
    price: Field = models.DecimalField(
        max_digits=8, decimal_places=2, blank=True, null=True
    )

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name} / {self.theatre.name}"


class SubscriptionPayment(models.Model):
    """Payment for subscription service."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    start = models.DateField()
    end = models.DateField()
    amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.id=} {self.start=} {self.end=} {self.amount=}"


class Subscription(models.Model):
    """Streaming subscription."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    start = models.DateField()
    end = models.DateField()
    payments = models.ManyToManyField(
        SubscriptionPayment,
        related_name="subscriptions",
        related_query_name="subscription",
    )

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.id=} {self.start=} {self.end=} {self.payments=}"


class Streaming(Media):
    """Media streamed via streaming service."""

    _type = MediaTypes.STREAMING

    streaming_service: Field = models.ForeignKey(
        Person,
        on_delete=models.PROTECT,
        related_name="streamings",
        related_query_name="streaming",
    )
    subscription: Field = models.ManyToManyField(
        Subscription,
        related_name="streamings",
        related_query_name="streaming",
    )

    def __str__(self) -> str:
        """Return string representation."""
        res = [f"{self.name}", f"{self.streaming_service.name}"]
        if self.season is not None:
            if self.sub_season is not None:
                res.append(f"season {self.season}.{self.sub_season}")
            else:
                res.append(f"season {self.season}")
        return " / ".join(res)


class Seen(models.Model):
    """Represents a viewing event."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date = models.DateField()
    watch_item = models.ForeignKey(
        WatchItem,
        on_delete=models.PROTECT,
        related_name="seens",
        related_query_name="seen",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["-date"]

    WatchItemTypes_ = WatchItemTypes

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.date}: {self.watch_item.title}"

    @property
    def media_type(self) -> str:
        """Return media type as string."""
        return f"{self.watch_item.medium.item_type}"
