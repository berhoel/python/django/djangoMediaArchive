#! /bin/bash

# Copyright © 2018 by Berthold Höllmann

# Task  : Generate class diagram.

# Author: Berthold Höllmann <berhoel@gmail.com>

# ID: $Id$
author="$Author$"
date="$Date$"
version="$Revision$"

curdir=$(pwd)

pushd ../../../

components="admin auth contenttypes sessions media sql_einstieg"

for comp in $components; do
	poetry run ./manage.py graph_models $comp >$curdir/$comp.dot
done

# all libraries, grouped
poetry run ./manage.py graph_models -a -g >$curdir/system.dot

popd

converters="circo neato fdp dot twopi"
#converters="dot"

for c in $converters; do
	for comp in $components; do
		$c $comp.dot -Tpdf -o${comp}_$c.pdf
	done
	$c system.dot -Tpdf -osystem_$c.pdf
done

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "sh genpic.sh"
# End:
