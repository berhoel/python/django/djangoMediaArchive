.. djangoMediaArchive documentation master file, created by
   sphinx-quickstart on Sun Sep 30 19:17:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to djangoMediaArchive's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. include:: ../README.rst

API Documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   berhoel.django
   berhoel.django.media.apps
   berhoel.django.media
   berhoel.django.media.models
   berhoel.django.media.admin
   berhoel.django.media.forms
   berhoel.django.media.urls
   berhoel.django.media.utils
   berhoel.django.media.views
   berhoel.django.media.widgets
   berhoel.django.media.migrations
   berhoel.django.media.settings

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
